package by.epam.java.task6.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import by.epam.java.task6.domain.Gem;
import by.epam.java.task6.parser.stax.StaxHandler;

@WebServlet("/MyServlet")
public class MyServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public void init(ServletConfig config) throws ServletException {
	System.out.println("servlet init method");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
	    throws ServletException, IOException {

	response.setContentType("text/html;charset=utf-8");
	PrintWriter out = response.getWriter();
	out.println(
		"<h2>Diamond Fund</h2><table style='border: 2px solid green; text-align: center; padding: 10px; margin: 20px'><th>Name</th><th>Preciousness"
			+ "</th><th>Origin</th><th>Colour</th><th>Transparency</th><th>Cutting</th><th>Value</th><tr>");

	XMLInputFactory inputFactory = XMLInputFactory.newInstance();

	InputStream input;
	List<Gem> gems = null;

	try {
	    input = new FileInputStream("D:\\Rabota\\DEV\\Java\\Java Web Development\\Task6\\resources\\gems2.xml");
	    // input = new FileInputStream("gems2.xml");
	    XMLStreamReader reader = inputFactory.createXMLStreamReader(input);

	    gems = StaxHandler.process(reader);

	} catch (FileNotFoundException | XMLStreamException e) {
	    e.printStackTrace();
	}

	for (Gem gem : gems) {

	    out.println("<td>" + gem.getName() + "</td><td>" + gem.getPreciousness() + "</td><td>" + gem.getOrigin()
		    + "</td><td style='background-color:" + gem.getColour() + "'>" + gem.getColour() + "</td><td>"
		    + gem.getTransparency() + "%" + "</td><td>" + gem.getCutting() + "</td><td>" + gem.getValue()
		    + " carats" + "</td></tr><tr>");
	}
    }
}
