package by.epam.java.task6.domain;

public class Gem {

    @Override
    public String toString() {
	return "Gem [name=" + name + ", preciousness=" + preciousness + ", origin=" + origin + ", colour=" + colour
		+ ", transparency=" + transparency + "%" + ", cutting=" + cutting + ", value=" + value + " carat" + "]";
    }

    private String name;
    private String preciousness;
    private String origin;
    private String colour;
    private int transparency;
    private int cutting;
    private double value;

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getPreciousness() {
	return preciousness;
    }

    public void setPreciousness(String preciousness) {
	this.preciousness = preciousness;
    }

    public String getOrigin() {
	return origin;
    }

    public void setOrigin(String origin) {
	this.origin = origin;
    }

    public String getColour() {
	return colour;
    }

    public void setColour(String colour) {
	this.colour = colour;
    }

    public int getTransparency() {
	return transparency;
    }

    public void setTransparency(int transparency) {
	this.transparency = transparency;
    }

    public int getCutting() {
	return cutting;
    }

    public void setCutting(int cutting) {
	this.cutting = cutting;
    }

    public double getValue() {
	return value;
    }

    public void setValue(double value) {
	this.value = value;
    }

}
