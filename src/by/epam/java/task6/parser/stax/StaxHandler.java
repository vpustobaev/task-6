package by.epam.java.task6.parser.stax;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.xml.sax.helpers.DefaultHandler;
import by.epam.java.task6.domain.Gem;
import by.epam.java.task6.parser.XMLElement;
import by.epam.java.task6.parser.XMLStreamConstants;

public class StaxHandler extends DefaultHandler {

    public static List<Gem> process(XMLStreamReader reader) throws XMLStreamException {

	XMLElement elementName = null;
	Gem gem = null;

	List<Gem> gems = null;
	while (reader.hasNext()) {

	    int type = reader.next();
	    switch (type) {

	    case XMLStreamConstants.START_ELEMENT:
		elementName = XMLElement.getXmlElement(reader.getLocalName());
		switch (elementName) {

		case GEMS:
		    gems = new ArrayList<Gem>();
		    break;

		case GEM:
		    gem = new Gem();
		    String name = reader.getAttributeValue(null, "preciousness");
		    gem.setPreciousness(name);
		    break;

		default:
		    break;
		}

		break;

	    case XMLStreamConstants.CHARACTERS:
		String text = reader.getText().trim();
		if (text.isEmpty()) {
		    break;
		}
		switch (elementName) {

		case NAME:
		    gem.setName(text);
		    break;
		case ORIGIN:
		    gem.setOrigin(text);
		    break;
		case COLOUR:
		    gem.setColour(text);
		    break;
		case TRANSPARENCY:
		    gem.setTransparency(new Integer(text));
		    break;
		case CUTTING:
		    gem.setCutting(new Integer(text));
		    break;

		case VALUE:
		    gem.setValue(new Double(text));
		    break;

		default:
		    break;
		}
		break;

	    case XMLStreamConstants.END_ELEMENT:
		elementName = XMLElement.getXmlElement(reader.getLocalName());

		switch (elementName) {

		case GEM:
		    gems.add(gem);
		    break;
		default:
		    break;

		}
	    }
	}
	return gems;
    }
}
