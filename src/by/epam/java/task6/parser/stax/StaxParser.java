package by.epam.java.task6.parser.stax;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import by.epam.java.task6.domain.Gem;

public class StaxParser {

    public static void main(String[] args) {

	XMLInputFactory inputFactory = XMLInputFactory.newInstance();

	InputStream input;
	try {
	    input = new FileInputStream("resources/gems2.xml");
	    XMLStreamReader reader = inputFactory.createXMLStreamReader(input);

	    List<Gem> gems = StaxHandler.process(reader);

	    for (int i = 0; i < gems.size(); i++) {

		System.out.println(gems.toArray()[i]);
	    }

	} catch (FileNotFoundException | XMLStreamException e) {
	    e.printStackTrace();
	}

    }

}
