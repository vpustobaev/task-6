package by.epam.java.task6.parser;

public enum XMLElement {

    GEMS, GEM, PRECIOUSNESS, NAME, ORIGIN, VISUAL_PARAMETERS, COLOUR, TRANSPARENCY, CUTTING, VALUE;

    public static XMLElement getXmlElement(String element) {

	switch (element) {

	case "gems":
	    return GEMS;
	case "gem":
	    return GEM;
	case "preciousness":
	    return PRECIOUSNESS;
	case "name":
	    return NAME;
	case "origin":
	    return ORIGIN;
	case "visual_parameters":
	    return VISUAL_PARAMETERS;
	case "colour":
	    return COLOUR;
	case "transparency":
	    return TRANSPARENCY;
	case "cutting":
	    return CUTTING;
	case "value":
	    return VALUE;

	default:
	    throw new EnumConstantNotPresentException(XMLElement.class, element);

	}

    }

}
