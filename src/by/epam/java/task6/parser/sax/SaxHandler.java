package by.epam.java.task6.parser.sax;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import by.epam.java.task6.domain.Gem;
import by.epam.java.task6.parser.XMLElement;

public class SaxHandler extends DefaultHandler {

    private String text;
    private StringBuilder builder;
    private Gem gem;
    private List<Gem> gems;

    @Override
    public void characters(char[] buffer, int start, int length) throws SAXException {
	text = builder.append(buffer, start, length).toString().trim().toUpperCase();
    }

    @Override
    public void endDocument() throws SAXException {
	for (int i = 0; i < gems.size(); i++) {
	    System.out.println(gems.toArray()[i]);
	}
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
	XMLElement element = XMLElement.valueOf(qName.toUpperCase().replace("-", "_"));

	switch (element) {

	case GEM:
	    gems.add(gem);
	    break;
	case NAME:
	    gem.setName(text);
	    break;
	case ORIGIN:
	    gem.setOrigin(text);
	    break;
	case COLOUR:
	    gem.setColour(text);
	    break;
	case TRANSPARENCY:
	    gem.setTransparency(new Integer(text));
	    break;
	case CUTTING:
	    gem.setCutting(new Integer(text));
	    break;
	case VALUE:
	    gem.setValue(new Double(text));
	    break;

	default:
	    break;
	}
    }

    @Override
    public void startDocument() throws SAXException {
	gems = new ArrayList<Gem>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

	builder = new StringBuilder();

	if ("gem".equals(qName)) {
	    gem = new Gem();

	}

    }
}
