package by.epam.java.task6.parser.sax;

import java.io.IOException;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class SaxParser {

    public static void main(String[] args) {

	try {
	    SaxHandler handler = new SaxHandler();
	    XMLReader reader = XMLReaderFactory.createXMLReader();
	    reader.setContentHandler(handler);
	    reader.parse("resources/gems2.xml");
	} catch (SAXException e) {

	    e.printStackTrace();
	} catch (IOException e) {

	    e.printStackTrace();
	}
    }
}
