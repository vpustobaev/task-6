package by.epam.java.task6.parser.dom;

import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DomParser {

    public static void main(String[] args) {

	try {
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    Document document = builder.parse("resources/gems2.xml");
	    Element root = document.getDocumentElement();

	    System.out.println("Root element :" + root.getNodeName());
	    NodeList nList = document.getElementsByTagName("gems");
	    System.out.println(nList.getLength());

	    for (int i = 0; i < nList.getLength(); i++) {

		System.out.println("List of elements: " + nList.item(i).getTextContent());

	    }
	    
	    

	} catch (ParserConfigurationException e) {

	    e.printStackTrace();
	} catch (SAXException e) {

	    e.printStackTrace();
	} catch (IOException e) {

	    e.printStackTrace();
	}

    }
}
