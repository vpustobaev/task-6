<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:local="http://www.epam.by/java/task5">
	<xsl:template match="/">
		<html>
			<body>
				<h2>Diamant Fund</h2>
				<table border="1">
					<tr bgcolor="#9acd32">
						<th style="text-align:left">Name</th>
						<th style="text-align:left">Preciousness</th>
						<th style="text-align:left">Origin</th>
						<th style="text-align:left">Colour</th>
						<th style="text-align:left">Transparency, in %</th>
						<th style="text-align:left">Cutting, number of edges</th>
						<th style="text-align:left">Value, in carat</th>
					</tr>
					<xsl:for-each select="local:gems/local:gem">
						<tr>
							<td>
								<xsl:value-of select="local:name" />
							</td>

							<td>
								<xsl:value-of select="@preciousness" />
							</td>

							<td>
								<xsl:value-of select="local:origin" />
							</td>
							<td>
								<xsl:value-of select="local:visual_parameters/local:colour" />
							</td>
							<td>
								<xsl:value-of select="local:visual_parameters/local:transparency" />
							</td>
							<td>
								<xsl:value-of select="local:visual_parameters/local:cutting" />
							</td>
							<td>
								<xsl:value-of select="local:value" />
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>